#DouDiZhuModel 斗地主游戏模型(服务器+cocos2d-js)
*   简化的斗地主服务器逻辑模型
*   全部操作使用队列处理，无显式锁，提高效率，降低理解难度。
*   特有的协议代码自动生成功能
*   使用cocos2d-js实现了简陋版的ui界面

展示地址(不稳定，随时可能失效)
[http://ddzgame.oschina.mopaas.com/](http://ddzgame.oschina.mopaas.com/)
